# data-argo deployment

<!-- BADGIE TIME -->

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)

<!-- END BADGIE TIME -->

Argo Workflows deployment on GKE Autopilot.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## About

Argo is a Kubernetes-native workflow engine, kind of like a super CI/CD system.
You can use it to run [native YAML
workflows](https://argo-workflows.readthedocs.io/en/latest/walk-through/hello-world/)
or Python workflows via a third-party library like
[Hera](https://github.com/argoproj-labs/hera).

## Environments

Argo Workflows is deployed at the following locations:

| Environment    |                                                     Location |
| -------------- | -----------------------------------------------------------: |
| **Production** |                 [`argo.dsri.cloud`](https://argo.dsri.cloud) |
| **Staging**    | [`staging-argo.dsri.cloud`](https://staging-argo.dsri.cloud) |

## Usage

### Install the Argo CLI

Argo CLI downloads can be found on the [GitHub
Releases](https://github.com/argoproj/argo-workflows/releases/) page.

Validate your installation with `argo version`:

```bash
argo version
```

```console
$ argo version
argo: v3.5.8
  BuildDate: 2024-06-18T04:41:19Z
  GitCommit: 3bb637c0261f8c08d4346175bb8b1024719a1f11
  GitTreeState: clean
  GitTag: v3.5.8
  GoVersion: go1.21.11
  Compiler: gc
  Platform: linux/amd64
```

### Log in with SSO

Argo will redirect to the Login page when not authenticated.

There will be a **LOGIN** button on the left side, under the following text:

> If your organisation has configured **single sign-on**:

Click **LOGIN**. You will be redirected to Google's login portal. Select your
`dsri.org` account. You will be redirected back to Argo and be logged in.

### Argo namespaces

The following namespaces are available for running workflows:

- `evaluations`

### Create an env.sh file

To connect to production:

```bash
# env.sh
export ARGO_SERVER=argo.dsri.cloud:443
export ARGO_SECURE=true
export ARGO_INSECURE_SKIP_VERIFY=false
export KUBECONFIG=/dev/null
export ARGO_NAMESPACE=evaluations
export ARGO_HTTP1=true
export ARGO_TOKEN="Bearer ......"
```

### Retrieve the authorization token

The value of the `ARGO_TOKEN` variable can be found in Firefox according to the
following steps:

- Open the **Inspect** tool (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>I</kbd>),

- Select the **Storage** tab,

- Select **Cookies** in the sidebar,

- Double-click the **Value** of **authorization**, then

- Copy/paste the token and set as the value of `ARGO_TOKEN`.

### Create the workflow

Here is an example YAML workflow. It can be saved by any name. For the sake of
example, we've named this one `workflow.yaml`:

```yaml
# workflow.yaml
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: detect-secrets-
  namespace: evaluations
spec:
  entrypoint: main
  serviceAccountName: evaluations-login
  templates:
    - name: main
      container:
        image: alpine:latest
        command: [sh, -euxc]
        args:
          - |-
            apk add --no-cache git py3-pip
            pip3 install --break-system-packages detect-secrets
            git clone --depth 1 https://gitlab.com/buildgarden/pipelines/python.git build
            cd build
            git ls-files | detect-secrets-hook --baseline .secrets.baseline
```

### Run the workflow

Source the `env.sh` file to configure the `argo` CLI.

```bash
$ argo submit workflow.yaml
```

### Work with secrets

Secrets can be added either manually or via Terraform in your namespace:

```bash
kubectl create secret generic gitlab --from-literal=gitlab=XXX
```

The `env` field can be added to a `container` field to use the secret.

```yaml
env:
  - name: GITLAB_AUTH_TOKEN
    valueFrom:
      secretKeyRef:
        name: gitlab
        key: gitlab
```

See
[Secrets](https://argo-workflows.readthedocs.io/en/stable/walk-through/secrets/)
for more information.

## Troubleshooting

If you see the following message, it means your authorization token has expired:

```
FATA[2024-08-14T22:40:50.036Z] Failed to submit workflow: rpc error: code =
Unauthenticated desc = failed to validate claims: go-jose/go-jose/jwt:
validation failed, token is expired (exp)
```

Repeat the login steps.

<!-- prettier-ignore-start -->
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 5.34.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.14.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.31.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | ~> 3.6.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | 5.34.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | 2.14.1 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.31.0 |
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.argo](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_deployment_v1.adminer](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/deployment_v1) | resource |
| [kubernetes_ingress_v1.adminer](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_manifest.production_issuer](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_manifest.staging_issuer](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_namespace.argo](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_namespace.evaluations](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_namespace_v1.adminer](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace_v1) | resource |
| [kubernetes_role.evaluations](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role) | resource |
| [kubernetes_role_binding.evaluations](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding) | resource |
| [kubernetes_secret.argo_artifacts_s3](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_secret.argo_db](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_secret.argo_oauth](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_secret.cloudflare_api_token](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_secret.evaluations_login](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_secret.user_default_login](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_service_account.evaluations_login](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account) | resource |
| [kubernetes_service_account.user_default_login](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account) | resource |
| [kubernetes_service_account_v1.adminer](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account_v1) | resource |
| [kubernetes_service_v1.adminer](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_v1) | resource |
| [google_client_config.provider](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |
| [google_container_cluster.data](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/container_cluster) | data source |
| [terraform_remote_state.data_cluster](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_argo_oauth_clientID"></a> [argo\_oauth\_clientID](#input\_argo\_oauth\_clientID) | n/a | `string` | n/a | yes |
| <a name="input_argo_oauth_token"></a> [argo\_oauth\_token](#input\_argo\_oauth\_token) | n/a | `string` | n/a | yes |
| <a name="input_argo_s3_access_key"></a> [argo\_s3\_access\_key](#input\_argo\_s3\_access\_key) | n/a | `string` | n/a | yes |
| <a name="input_argo_s3_bucket"></a> [argo\_s3\_bucket](#input\_argo\_s3\_bucket) | n/a | `string` | n/a | yes |
| <a name="input_argo_s3_secret_key"></a> [argo\_s3\_secret\_key](#input\_argo\_s3\_secret\_key) | n/a | `string` | n/a | yes |
| <a name="input_cloudflare_api_token"></a> [cloudflare\_api\_token](#input\_cloudflare\_api\_token) | n/a | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | n/a | `string` | n/a | yes |
| <a name="input_google_cloud_service_account_file"></a> [google\_cloud\_service\_account\_file](#input\_google\_cloud\_service\_account\_file) | n/a | `string` | n/a | yes |
| <a name="input_hostname_prefix"></a> [hostname\_prefix](#input\_hostname\_prefix) | n/a | `string` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | n/a | `string` | n/a | yes |
| <a name="input_remote_state_password"></a> [remote\_state\_password](#input\_remote\_state\_password) | n/a | `string` | n/a | yes |
| <a name="input_remote_state_user"></a> [remote\_state\_user](#input\_remote\_state\_user) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- prettier-ignore-end -->
