# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0
locals {
  adminer_name = "adminer"
  adminer_labels = {
    "app.kubernetes.io/instance" = local.adminer_name
  }

  adminer_design  = ""
  adminer_plugins = ""

  adminer_hostname = "${var.hostname_prefix}adminer.dsri.cloud"

  adminer_user_id = 2000
}

resource "kubernetes_namespace_v1" "adminer" {
  metadata {
    name = local.adminer_name
  }
}

resource "kubernetes_service_account_v1" "adminer" {
  metadata {
    name      = local.adminer_name
    namespace = kubernetes_namespace_v1.adminer.metadata.0.name
    labels    = local.adminer_labels
  }
}

resource "kubernetes_deployment_v1" "adminer" {
  metadata {
    name      = local.adminer_name
    namespace = kubernetes_namespace_v1.adminer.metadata.0.name
    labels    = local.adminer_labels
  }

  spec {
    replicas = 1

    selector {
      match_labels = local.adminer_labels
    }

    template {
      metadata {
        labels = local.adminer_labels
      }

      spec {
        init_container {
          name  = "html"
          image = "ghcr.io/shyim/adminerevo:4.8.4"

          command = ["/bin/sh", "-ec"]
          args = [
            <<-EOF
            cp -a /var/www/html/* /build/

            if [ -n "$ADMINER_DESIGN" ]; then
              ln -sf "designs/$ADMINER_DESIGN/adminer.css" /build/adminer.css
            fi

            mkdir -p /build/plugins-enabled

            number=1
            for PLUGIN in $ADMINER_PLUGINS; do
            	php /var/www/html/plugin-loader.php "$PLUGIN" > /build/plugins-enabled/$(printf "%03d" $number)-$PLUGIN.php
            	number=$(($number+1))
            done
            EOF
          ]

          env {
            name  = "ADMINER_DESIGN"
            value = local.adminer_design
          }

          env {
            name  = "ADMINER_PLUGINS"
            value = local.adminer_plugins
          }

          resources {
            limits = {
              "cpu"               = "100m"
              "ephemeral-storage" = "2Gi"
              "memory"            = "200Mi"
            }
            requests = {
              "cpu"               = "100m"
              "ephemeral-storage" = "2Gi"
              "memory"            = "200Mi"
            }
          }

          security_context {
            allow_privilege_escalation = false

            capabilities {
              drop = ["ALL"]
            }

            privileged                = false
            read_only_root_filesystem = false
            run_as_group              = local.argo_user_id
            run_as_non_root           = true
            run_as_user               = local.argo_user_id

            seccomp_profile {
              type = "RuntimeDefault"
            }
          }

          volume_mount {
            mount_path = "/build"
            name       = "html"
          }
        }

        container {
          name              = "main"
          image             = "ghcr.io/shyim/adminerevo:4.8.4"
          image_pull_policy = "IfNotPresent"

          command = ["php"]
          args    = ["-S", "[::]:8080", "-t", "/var/www/html"]

          env {
            name  = "ADMINER_DESIGN"
            value = local.adminer_design
          }

          env {
            name  = "ADMINER_PLUGINS"
            value = local.adminer_plugins
          }

          port {
            name           = "http"
            container_port = 8080
            protocol       = "TCP"
          }

          resources {
            limits = {
              "cpu"               = "150m"
              "ephemeral-storage" = "1Gi"
              "memory"            = "312Mi"
            }
            requests = {
              "cpu"               = "150m"
              "ephemeral-storage" = "1Gi"
              "memory"            = "312Mi"
            }
          }

          security_context {
            allow_privilege_escalation = false
            capabilities {
              drop = ["ALL"]
            }
            privileged                = false
            read_only_root_filesystem = false
            run_as_group              = local.argo_user_id
            run_as_non_root           = true
            run_as_user               = local.argo_user_id
          }

          volume_mount {
            mount_path = "/var/www/html"
            name       = "html"
            read_only  = true
          }
        }

        container {
          name  = "proxy"
          image = "gcr.io/cloud-sql-connectors/cloud-sql-proxy:2.13.0"

          args = [
            "--private-ip",
            "${local.argo_db.instance.connection_name}?port=5001",
            "${local.pipeline_db.instance.connection_name}?port=5002",
          ]

          resources {
            limits = {
              "cpu"               = "100m"
              "ephemeral-storage" = "1Gi"
              "memory"            = "200Mi"
            }
            requests = {
              "cpu"               = "100m"
              "ephemeral-storage" = "1Gi"
              "memory"            = "200Mi"
            }
          }

          security_context {
            allow_privilege_escalation = false
            capabilities {
              drop = ["ALL"]
            }
            privileged                = false
            read_only_root_filesystem = false
            run_as_group              = local.argo_user_id
            run_as_non_root           = true
            run_as_user               = local.argo_user_id
          }

          volume_mount {
            mount_path = "/var/www/html"
            name       = "html"
            read_only  = true
          }
        }

        security_context {
          fs_group        = local.argo_user_id
          run_as_group    = local.argo_user_id
          run_as_non_root = true
          run_as_user     = local.argo_user_id

          seccomp_profile {
            type = "RuntimeDefault"
          }
        }

        service_account_name = kubernetes_service_account_v1.adminer.metadata.0.name

        toleration {
          effect   = "NoSchedule"
          key      = "kubernetes.io/arch"
          operator = "Equal"
          value    = "amd64"
        }

        volume {
          name = "html"
          empty_dir {}
        }
      }
    }
  }

  lifecycle {
    ignore_changes = [
      metadata.0.annotations["autopilot.gke.io/resource-adjustment"],
      metadata.0.annotations["autopilot.gke.io/warden-version"],
    ]
  }
}

resource "kubernetes_service_v1" "adminer" {
  metadata {
    name      = local.adminer_name
    namespace = kubernetes_namespace_v1.adminer.metadata.0.name
    labels    = local.adminer_labels
  }

  spec {
    port {
      port        = 8080
      target_port = "http"
      protocol    = "TCP"
      name        = "http"
    }

    selector = local.adminer_labels
    type     = "ClusterIP"
  }
}

resource "kubernetes_ingress_v1" "adminer" {
  metadata {
    name      = local.adminer_name
    namespace = kubernetes_namespace_v1.adminer.metadata.0.name
    labels    = local.adminer_labels
    annotations = {
      "cert-manager.io/cluster-issuer"          = "letsencrypt-production"
      "nginx.ingress.kubernetes.io/auth-url"    = "https://$host/oauth2/auth"
      "nginx.ingress.kubernetes.io/auth-signin" = "https://$host/oauth2/sign_in?rd=$escaped_request_uri"
    }
  }

  spec {
    ingress_class_name = "nginx"
    rule {
      host = local.adminer_hostname
      http {
        path {
          path      = "/"
          path_type = "ImplementationSpecific"
          backend {
            service {
              name = kubernetes_service_v1.adminer.metadata.0.name
              port {
                name = "http"
              }
            }
          }
        }
      }
    }
    tls {
      hosts       = [local.adminer_hostname]
      secret_name = "${local.adminer_name}-ingress-tls"
    }
  }
}
