# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "argo" {
  metadata {
    name = "argo"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

resource "kubernetes_namespace" "evaluations" {
  metadata {
    name = "evaluations"
  }
}

resource "kubernetes_secret" "argo_oauth" {
  metadata {
    name      = "argo-server-sso"
    namespace = kubernetes_namespace.argo.metadata.0.name
  }
  data = {
    "client-id"         = var.argo_oauth_clientID
    "data-argo-pasword" = var.argo_oauth_token
  }
}

resource "kubernetes_secret" "argo_db" {
  metadata {
    name      = "argo-db"
    namespace = kubernetes_namespace.argo.metadata.0.name
  }
  data = {
    "username" = "argo"
    "password" = local.argo_db.users.argo
  }
}

resource "kubernetes_secret" "argo_artifacts_s3" {
  metadata {
    name      = "argo-artifacts-s3"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
  }
  data = {
    "access-key" = var.argo_s3_access_key
    "secret-key" = var.argo_s3_secret_key
  }
}

locals {
  argo_user_id = 8737

  cloud_sql_proxy_container = {
    name  = "cloud-sql-proxy"
    image = "gcr.io/cloud-sql-connectors/cloud-sql-proxy:2.13.0"
    args = [
      "--unix-socket", "/cloudsql",
      "--private-ip",
      local.argo_db.instance.connection_name,
    ]
    resources = {
      limits = {
        cpu    = "250m"
        memory = "500Mi"
      }
      requests = {
        cpu    = "250m"
        memory = "500Mi"
      }
    }
    securityContext = {
      allowPrivilegeEscalation = false
      capabilities = {
        drop = ["ALL"]
      }
      readOnlyRootFilesystem = true
      runAsNonRoot           = true
    }
    volumeMounts = [local.cloud_sql_volume_mount]
  }

  cloud_sql_volume_mount = {
    name      = "cloudsql"
    mountPath = "/cloudsql"
  }

  cloud_sql_volume = {
    name     = "cloudsql"
    emptyDir = {}
  }
}

#https://artifacthub.io/packages/helm/bitnami/argo-workflows
resource "helm_release" "argo" {
  name      = "argo"
  namespace = kubernetes_namespace.argo.metadata.0.name

  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-workflows"
  version    = "0.41.13"

  force_update  = true
  wait          = false
  wait_for_jobs = false

  values = [yamlencode({

    artifactRepository = {
      archiveLogs = true
      s3 = {
        accessKeySecret = {
          name = kubernetes_secret.argo_artifacts_s3.metadata.0.name
          key  = "access-key"
        }
        secretKeySecret = {
          name = kubernetes_secret.argo_artifacts_s3.metadata.0.name
          key  = "secret-key"
        }
        insecure = false
        bucket   = var.argo_s3_bucket
        endpoint = "c71f34685220d85ddd7a0e531ba2635d.r2.cloudflarestorage.com"
        region   = "auto"
      }
    }

    controller = {
      extraContainers = [local.cloud_sql_proxy_container]

      links = [{
        name  = "Pod metrics"
        scope = "pod"
        url   = "https://console.cloud.google.com/kubernetes/deployment/${local.region}/${local.cluster_name}/$${metadata.namespace}/$${metadata.name}/overview?project=${var.project}"
      }]

      persistence = {
        archive           = true
        nodeStatusOffLoad = true
        postgresql = {
          host      = "/cloudsql/${local.argo_db.instance.connection_name}"
          database  = "argo"
          tableName = "workflows"
          userNameSecret = {
            name = kubernetes_secret.argo_db.metadata.0.name
            key  = "username"
          }
          passwordSecret = {
            name = kubernetes_secret.argo_db.metadata.0.name
            key  = "password"
          }
          ssl     = false
          sslMode = "disable"
        }
      }

      podSecurityContext = {
        fsGroup = local.argo_user_id
      }

      securityContext = {
        readOnlyRootFilesystem = true
        runAsUser              = local.argo_user_id
        runAsGroup             = local.argo_user_id
      }

      volumeMounts = [local.cloud_sql_volume_mount]
      volumes      = [local.cloud_sql_volume]

      workflowDefaults = {
        spec = {
          entrypoint  = "main"
          parallelism = 10
          podGC = {
            deleteDelayDuration = "600s"
            strategy            = "OnPodCompletion"
          }
          ttlStrategy = {
            secondsAfterCompletion = 86400
            secondsAfterFailure    = 86400
            secondsAfterSuccess    = 86400
          }
        }
      }
    }

    server = {
      authModes = ["sso"]

      extraContainers = [local.cloud_sql_proxy_container]

      extraEnv = [{
        name  = "SSO_DELEGATE_RBAC_TO_NAMESPACE"
        value = "true"
      }]

      ingress = {
        annotations = {
          "cert-manager.io/cluster-issuer" = "letsencrypt-production"
        }
        enabled          = true
        ingressClassName = "nginx"
        hosts            = [local.argo_hostname]
        tls = [{
          secretName = "${var.environment}-data-argo-tls"
          hosts      = [local.argo_hostname]
          }
        ]
      }

      podSecurityContext = {
        fsGroup = local.argo_user_id
      }

      securityContext = {
        readOnlyRootFilesystem = true
        runAsUser              = local.argo_user_id
        runAsGroup             = local.argo_user_id
      }

      sso = {
        enabled     = true
        redirectUrl = "https://${local.argo_hostname}/oauth2/callback"
        clientID = {
          name = "argo-server-sso"
          key  = "client-id"
        }
        clientSecret = {
          name = "argo-server-sso"
          key  = "data-argo-pasword"
        }
        scopes = ["email", "profile"]
        rbac = {
          enabled = true
        }
      }

      volumeMounts = [local.cloud_sql_volume_mount]
      volumes      = [local.cloud_sql_volume]
    }
  })]
}
