# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/google" {
  version     = "5.34.0"
  constraints = "~> 5.34.0"
  hashes = [
    "h1:RqHQ8SX0xtvAgAcUHvRaGegAqxn2AOXnLvh3jwTGuTM=",
    "zh:0356710cbf9f2802fe70d9bfb7b0dabdd6b818555b31e51f674335dddf4b3ade",
    "zh:144c76328799203c2949d776659d18cc72db922e83cab891aac1214b27369957",
    "zh:5e40e3ad4a9568e3bcd77440b23aefb0d8d77e46edab2dcb188a730cdf79744d",
    "zh:9d1c1a74cdffce89516b6ce6228d8f826162341c072425b096586728cf794119",
    "zh:a8a6d85dad4fdfdaf893c5821bae665e14ab00f77a20315f617373e7ec1e0359",
    "zh:aeb80a2027e58736f2a3acdf2c97f77e972e2db0d7a692bfdba548ce529d0b9c",
    "zh:d084ee87336f4ef2d081a0021befd830291899a05544a37db43ff2a3e82c2f70",
    "zh:d76e52dff50dbaba004edd787528155492d34e8576e5460f69a4a218403729c4",
    "zh:d7d6468f2aca06412b162d1a91f98ebacb55b71699a000b4ce451c68ad98f83e",
    "zh:f376ac7ffb8650713f8555a2508b1dac87b27df1829948af5d79001717acdfcc",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.14.1"
  constraints = "~> 2.14.0"
  hashes = [
    "h1:WB6ahIjX1hgmeB8Edn6AAImw5LrOvPxJP7pi0KhhEz0=",
    "zh:3e0c73080563611451e7de1b5c28ce945da7027aac619df6506f8d5c420ba2d4",
    "zh:5b8760f2b9798cf3160a4db4001463a3f1001359c74a6ef601a81bfbc4c4bd74",
    "zh:64ab4ad0e650e717bf5932717a94b6f6be790b6f7f93b51da371c3383933e233",
    "zh:7173f961833c564d83ed4994aaaefa7be849c2a63bbac3cd288ccf3be763ca21",
    "zh:7ffce2dd580e15b98e0f2601d1da3bf8b4adeabe4c4de513f67f480871e0c594",
    "zh:962433200cefbb96d009c1fa858e4a3068926ba85de60fa0a0cf321ce7e26697",
    "zh:aba9b11d9e48a18fd3290f164ae4748d44f2a734402bc48b86a140edd64a60cd",
    "zh:b9f7080226ab201cd80c8a1e6f5e5bb6d93cfd84bcf693845d35963933a37012",
    "zh:dd1771c46668c1615900ab559a7e4dea552c615f3eff5cf611a85b7ede18bdc8",
    "zh:f85abfcfe40475c878a7135e4abbb152454e9dc98b8e2b7b6928e7f9402ad127",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.31.0"
  constraints = "~> 2.31.0"
  hashes = [
    "h1:z2qlqn6WbrjbezwQo4vvlwAgVUGz59klzDU4rlYhYi8=",
    "zh:0dd25babf78a88a61dd329b8c18538a295ea63630f1b69575e7898c89307da39",
    "zh:3138753e4b2ce6e9ffa5d65d73e9236169ff077c10089c7dc71031a0a139ff6d",
    "zh:644f94692dc33de0bb1183c307ae373efbf4ef4cb92654ccc646a5716edf9593",
    "zh:6cc630e43193220b1599e3227286cc4e3ca195910e8c56b6bacb50c5b5176dbf",
    "zh:764173875e77aa482da4dca9fec5f77c455d028848edfc394aa7dac5dfed6afd",
    "zh:7b1d380362d50ffbb3697483036ae351b0571e93b33754255cde6968e62b839f",
    "zh:a1d93ca3d8d1ecdd3b69242d16ff21c91b34e2e98f02a3b2d02c908aeb45189b",
    "zh:b471d0ab56dbf19c95fba68d2ef127bdb353be96a2be4c4a3dcd4d0db4b4180a",
    "zh:d610f725ded4acd3d31a240472bb283aa5e657ed020395bdefea18d094b8c2bf",
    "zh:d7f3ddd636ad5af6049922f212feb24830b7158410819c32073bf81c359cd2fa",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.6.2"
  constraints = "~> 3.6.2"
  hashes = [
    "h1:PXvoOj9gj+Or+9k0tQWCQJKxnsVO0GqnQwVahgwRrsU=",
    "zh:1f27612f7099441526d8af59f5b4bdcc35f46915df5d243043d7337ea5a3e38a",
    "zh:2a58e66502825db8b4b96116c04bd0323bca1cf1f5752bdd8f9c26feb84d3b1e",
    "zh:4f0a4fa479e29de0c3c90146fd58799c097f7a55401cb00560dd4e9b1e6fad9d",
    "zh:9c93c0fe6ef685513734527e0c8078636b2cc07591427502a7260f4744b1af1d",
    "zh:a466ff5219beb77fb3b18a3d7e7fe30e7edd4d95c8e5c87f4f4e3fe3eeb8c2d7",
    "zh:ab33e6176d0c757ddb31e40e01a941e6918ad10f7a786c8e8e4f35e5cff81c96",
    "zh:b6eabf377a1c12cb3f9ddd97aacdd5b49c1646dc959074124f81d40fcd216d7e",
    "zh:ccec5d03d0d1c0f354be299cdd6a417b2700f1a6781df36bcce77246b2f57e50",
    "zh:d2a7945eeb691fdd2b1474da76ddc2d1655e2aedbb14b57f06d4f5123d47adf9",
    "zh:ed62351f4ad9d1469c6798b77dee5f63b18b29c473620a0046ba3d4f111b621d",
  ]
}
