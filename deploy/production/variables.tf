# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "google_cloud_service_account_file" {
  type    = string
  default = "credentials.json"
}

variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}

variable "argo_oauth_token" {
  type      = string
  sensitive = true
}

variable "argo_oauth_clientID" {
  type      = string
  sensitive = true
}

variable "argo_s3_access_key" {
  type      = string
  sensitive = true
}

variable "argo_s3_secret_key" {
  type      = string
  sensitive = true
}

variable "remote_state_user" {
  type      = string
  sensitive = true
}

variable "remote_state_password" {
  type      = string
  sensitive = true
}
