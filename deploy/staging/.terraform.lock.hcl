# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/opentofu/google" {
  version     = "6.1.0"
  constraints = "~> 6.1.0"
  hashes = [
    "h1:S40z2lx6fSUgARSezOw40+rSdTX1lHOaWowj6jTnx18=",
    "zh:026c72f41e7e4676b31487b7381362bcbdb33de44453e855cdc60c01df181a5d",
    "zh:0e9dec23eaffaa984fbebc89bef139ea8af362562185cff42f3e71f9aeb9ed6f",
    "zh:27f5b4c0381110bfaa01ca214bff2c31e6f18ac74c9d9e918afc49d599baf32e",
    "zh:34a9871dc1c43437972cfc87f5bd76ce7dadf6bbb5042807c7ebaa689b11a681",
    "zh:4d2eec408057faed89424c8a02439181283fb15cbb1d9e90dab29aa171e50874",
    "zh:54891b58e43c493402dedfb13574856a06b40de0df4c2543eeb55014721bf60f",
    "zh:7aa6d6362da50c220700652edb87575daf14517b4c380b8dd8191e1ff69f2420",
    "zh:dd21c999bed05b5214186ae23b8d8e7a4575856f30dbbf9848e2778016c0b8c0",
    "zh:f62c7685f75445a28001ededd260dc2056f84f38ca087dc61803322672adffd9",
    "zh:f9cd3a06a692f4e9c262e0d012480a19b355dc8769c16a4ec257de7c668a0965",
  ]
}

provider "registry.opentofu.org/opentofu/helm" {
  version     = "2.15.0"
  constraints = "~> 2.15.0"
  hashes = [
    "h1:Cak4To+L1FxO1qkb2Gaonyjtdpu+vglc6NHynyUHylw=",
    "zh:21394ae3ec6f8ccda74688f8aeb979c03c9c52b60b5d0ada10521b5a75ae85af",
    "zh:248ba25e309432dc7a2a6049da9178731ae3884be1761c4e349c844ce5159d82",
    "zh:30dd6046b239f8b3788958475ad4db9b956c99ea71a0492fe6f2380d8d711ffc",
    "zh:40691066592cdd396226ff0ecd4153dce91799375282c3c8a13fdf21d616c73b",
    "zh:54b16f5ac335903f6bd6c7ba03c66b894940511a0d16c6ad92a16fe9ef80aaa8",
    "zh:9af1702deec999a8ba5fa379de6eb515bf8b045bb02a7f24e3aa1a559f88ec12",
    "zh:d057a371798b526b32d6985baaaf6e8126f14f23e1ebd65b44b970064c7790e5",
    "zh:de6fa77b4763ccdcf8d5546d54609299e3b0a2cfe3446e62d5cfa7806e2aa003",
    "zh:e2a21a57031a97abd3a61c09ffa84f4aae451329e876c2cd6597e02947ca1008",
    "zh:f8d12702874a935e0e2397bdb050f4d4c0d83fb4c0a9c7dfd1b49257605149bd",
  ]
}

provider "registry.opentofu.org/opentofu/kubernetes" {
  version     = "2.32.0"
  constraints = "~> 2.32.0"
  hashes = [
    "h1:ZRCFOIecOlTIrpf1O/kmbFfBMQe9r8/HwiiK9kP0KEk=",
    "zh:06d586c8fcd3ab8fe7f3ac99142ba48b9efbff8bebe05c52b3c7997f83146200",
    "zh:12ce862493717118a6bf68328448d09023a60344da25633e124423cdd734263e",
    "zh:33ee1cda5db58fd26576ba6be715282af30e04d25b38fd6752810fd206bc6422",
    "zh:8f4e13c726a5fb84244eff7740b20678e7fb2d5df6ebc759101d4c58fb069112",
    "zh:8fe15d350b5a018f535a93fa054bf4d05377a69f3b1e5cabe8c73d059a4b70cb",
    "zh:953fc8c8a92ff0defafd22ee0aec12d483d7b80685de6838e513d4de7170a651",
    "zh:a1ad6197105f9cda73c39f3b69dd688ec22708c736de05c03516561a88f4bbfc",
    "zh:c1d60898c269f42ece0b3672901001ba26338c865f83a39b116c0d6c0cd8dbc1",
    "zh:d26fcff2fda9421d9129fd407696481ecd2714ae3316e81ff977e2e40de068e5",
    "zh:dc616b73095755245f211af0989bfcf2f76b43196bf7f8982183e4e3b1c3f6f6",
  ]
}

provider "registry.opentofu.org/opentofu/random" {
  version     = "3.6.3"
  constraints = "~> 3.6.2"
  hashes = [
    "h1:Ry0Lr0zaoicslZlcUR4rAySPpl/a7QupfMfuAxhW3fw=",
    "zh:1bfd2e54b4eee8c761a40b6d99d45880b3a71abc18a9a7a5319204da9c8363b2",
    "zh:21a15ac74adb8ba499aab989a4248321b51946e5431219b56fc827e565776714",
    "zh:221acfac3f7a5bcd6cb49f79a1fca99da7679bde01017334bad1f951a12d85ba",
    "zh:3026fcdc0c1258e32ab519df878579160b1050b141d6f7883b39438244e08954",
    "zh:50d07a7066ea46873b289548000229556908c3be746059969ab0d694e053ee4c",
    "zh:54280cdac041f2c2986a585f62e102bc59ef412cad5f4ebf7387c2b3a357f6c0",
    "zh:632adf40f1f63b0c5707182853c10ae23124c00869ffff05f310aef2ed26fcf3",
    "zh:b8c2876cce9a38501d14880a47e59a5182ee98732ad7e576e9a9ce686a46d8f5",
    "zh:f27e6995e1e9fe3914a2654791fc8d67cdce44f17bf06e614ead7dfd2b13d3ae",
    "zh:f423f2b7e5c814799ad7580b5c8ae23359d8d342264902f821c357ff2b3c6d3d",
  ]
}
