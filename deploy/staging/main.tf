# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "root" {
  source = "../.."

  cloudflare_api_token              = var.cloudflare_api_token
  environment                       = "staging"
  google_cloud_service_account_file = var.google_cloud_service_account_file
  hostname_prefix                   = "staging-"
  argo_oauth_token                  = var.argo_oauth_token
  argo_oauth_clientID               = var.argo_oauth_clientID
  project                           = "staging-data-467236"

  argo_s3_bucket        = "staging-argo-artifacts"
  argo_s3_access_key    = var.argo_s3_access_key
  argo_s3_secret_key    = var.argo_s3_secret_key
  remote_state_user     = var.remote_state_user
  remote_state_password = var.remote_state_password
}
