# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/59358374/terraform/state/staging"
    lock_address   = "https://gitlab.com/api/v4/projects/59358374/terraform/state/staging/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/59358374/terraform/state/staging/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }
}
