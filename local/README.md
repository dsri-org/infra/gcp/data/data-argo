# Setup

Install the CLI on Linux:

```bash
curl -sSLO https://github.com/argoproj/argo-workflows/releases/download/v3.5.8/argo-linux-amd64.gz
gunzip argo-linux-amd64.gz
sudo install argo-linux-amd64 /usr/local/bin/argo
argo version
argo completion bash | sudo tee /etc/bash_completion.d/argo
```

Releases: https://github.com/argoproj/argo-workflows/releases/

Ignore the quickstart install. Use the Helm chart: https://artifacthub.io/packages/helm/bitnami/argo-workflows

Examples: https://github.com/argoproj/argo-workflows/tree/main/examples

Configmap: https://argo-workflows.readthedocs.io/en/latest/workflow-controller-configmap.yaml

Docs: https://argo-workflows.readthedocs.io/en/latest/

```bash
minikube start --cpus=6 --memory=8G
```

```bash
kubectl port-forward service/argo-argo-workflows-server 2746:80
```

```bash
ARGO_TOKEN="Bearer $(kubectl get secret argo-reader -o=jsonpath='{.data.token}' | base64 --decode)" ; echo $ARGO_TOKEN
```

```bash
terraform apply -auto-approve
```

```bash
argo submit test.yaml
```
