# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "argo" {
  metadata {
    name = "argo"
  }
}

resource "random_password" "argo_postgresql_password" {
  length  = 24
  special = false
}

resource "random_password" "argo_postgresql_root_password" {
  length  = 24
  special = false
}
#https://artifacthub.io/packages/helm/bitnami/argo-workflows
resource "helm_release" "argo" {
  name      = "argo"
  namespace = kubernetes_namespace.argo.metadata.0.name

  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "argo-workflows"
  version    = "9.1.6"

  values = [yamlencode({

  })]

  set_sensitive {
    name  = "postgresql.auth.password"
    value = random_password.argo_postgresql_password.result
  }

  set_sensitive {
    name  = "postgresql.auth.postgresPassword"
    value = random_password.argo_postgresql_root_password.result
  }
}
