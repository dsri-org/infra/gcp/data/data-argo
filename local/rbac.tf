# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_service_account" "reader" {
  metadata {
    name      = "argo-reader"
    namespace = kubernetes_namespace.argo.metadata.0.name
  }
}

# resource "kubernetes_role" "reader" {
#   metadata {
#     name = "argo-reader"
#     namespace = kubernetes_namespace.argo.metadata.0.name
#   }
#
#   rule {
#     api_groups     = ["argoproj.io"]
#     resources      = ["workflows", "workflowtemplates", "cronworkflows", "workflowtaskresults", "workfloweventbindings"]
#     verbs          = ["get", "list", "update", "watch"]
#   }
# }
#
# resource "kubernetes_role_binding" "reader" {
#   metadata {
#     name = "argo-reader"
#     namespace = kubernetes_namespace.argo.metadata.0.name
#   }
#
#   role_ref {
#     api_group = "rbac.authorization.k8s.io"
#     kind      = "Role"
#     name      = kubernetes_role.reader.metadata.0.name
#   }
#   subject {
#     kind      = "ServiceAccount"
#     name      = kubernetes_service_account.reader.metadata.0.name
#     namespace = kubernetes_namespace.argo.metadata.0.name
#   }
# }

resource "kubernetes_cluster_role" "reader" {
  metadata {
    name = "argo-reader"
  }

  rule {
    api_groups = ["argoproj.io"]
    resources  = ["workflows", "workflowtemplates", "cronworkflows", "workflowtaskresults", "workfloweventbindings", "clusterworkflowtemplates", "sensors", "eventsources"]
    verbs      = ["get", "list", "update", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["pods", "pods/log"]
    verbs      = ["get", "list", "patch"]
  }
}

resource "kubernetes_cluster_role_binding" "reader" {
  metadata {
    name = "argo-reader"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.reader.metadata.0.name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.reader.metadata.0.name
    namespace = kubernetes_namespace.argo.metadata.0.name
  }
}

resource "kubernetes_secret" "reader" {
  metadata {
    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account.reader.metadata.0.name
    }
    name      = "argo-reader"
    namespace = kubernetes_namespace.argo.metadata.0.name
  }
  type = "kubernetes.io/service-account-token"
}
