# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  deployment = "data-argo"
  region     = "us-central1"
  name       = "${var.environment}-${local.deployment}"

  argo_db       = data.terraform_remote_state.data_cluster.outputs.databases.argo
  argo_hostname = "${var.hostname_prefix}argo.dsri.cloud"

  pipeline_db = data.terraform_remote_state.data_cluster.outputs.databases.pipeline

  cluster_name = "${var.environment}-data-cluster"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  issuer_email = "service@dsri.org"
}
