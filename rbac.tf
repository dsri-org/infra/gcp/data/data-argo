# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_role" "evaluations" {
  metadata {
    namespace = kubernetes_namespace.evaluations.metadata.0.name
    name      = "evaluations"
  }

  rule {
    api_groups = ["argoproj.io"]
    resources  = ["workflows", "workflowtemplates", "cronworkflows", "workflowtaskresults", "workfloweventbindings", "clusterworkflowtemplates", "sensors", "eventsources"]
    verbs      = ["create", "delete", "get", "list", "patch", "update", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["pods", "pods/log"]
    verbs      = ["get", "list", "patch", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["configmaps", "secrets"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_role_binding" "evaluations" {
  metadata {
    namespace = kubernetes_namespace.evaluations.metadata.0.name
    name      = "evaluations"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.evaluations.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    name      = "evaluations-login"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
  }
}

#default read-only account to use if no rules match
resource "kubernetes_service_account" "user_default_login" {
  metadata {
    name      = "user-default-login"
    namespace = kubernetes_namespace.argo.metadata.0.name
    annotations = {
      "workflows.argoproj.io/rbac-rule"            = "true"
      "workflows.argoproj.io/rbac-rule-precedence" = "0"
    }
  }
}

resource "kubernetes_service_account" "evaluations_login" {
  metadata {
    name      = "evaluations-login"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
    annotations = {
      "workflows.argoproj.io/rbac-rule"            = "true"
      "workflows.argoproj.io/rbac-rule-precedence" = "1"
    }
  }
}

resource "kubernetes_secret" "evaluations_login" {
  type = "kubernetes.io/service-account-token"
  metadata {
    name      = "evaluations-login.service-account-token"
    namespace = kubernetes_namespace.evaluations.metadata.0.name
    annotations = {
      "kubernetes.io/service-account.name" = "evaluations-login"
    }
  }
}

resource "kubernetes_secret" "user_default_login" {
  type = "kubernetes.io/service-account-token"
  metadata {
    name      = "user-default-login.service-account-token"
    namespace = kubernetes_namespace.argo.metadata.0.name
    annotations = {
      "kubernetes.io/service-account.name" = "user-default-login"
    }
  }
}
