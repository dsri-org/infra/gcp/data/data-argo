# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

terraform {
  required_providers {
    google = {
      source  = "opentofu/google"
      version = "~> 6.1.0"
    }
    helm = {
      source  = "opentofu/helm"
      version = "~> 2.15.0"
    }
    kubernetes = {
      source  = "opentofu/kubernetes"
      version = "~> 2.32.0"
    }
    random = {
      source  = "opentofu/random"
      version = "~> 3.6.2"
    }
  }
  required_version = ">=1.0"
}
